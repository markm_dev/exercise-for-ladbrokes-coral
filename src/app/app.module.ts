import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainViewComponent } from './main-view/main-view.component';

import { FormsModule } from '@angular/forms';
import { TrackDetailsComponent } from './track-details/track-details.component';
import { CONSTS } from './consts'

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    TrackDetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [CONSTS],
  bootstrap: [AppComponent]
})
export class AppModule { }
