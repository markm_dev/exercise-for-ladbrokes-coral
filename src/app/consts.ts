import { Injectable } from '@angular/core';

@Injectable()
export class CONSTS {
  static SC_CLIENT_ID: string = '1fb0d04a94f035059b0424154fd1b18c';
  static CONFIG_SEARCH_PAGE_SIZE: number = 6;
  static CONFIG_MAX_HISTORY_LENGTH: number = 5;
  static STR_SEARCH_PLACEHOLDER: string = 'Search by track name';
  static STR_SEARCH_BTN: string = 'GO';
  static STR_HISTORY_TITLE: string = 'History';
  static STR_API_ERROR: string = 'Oops, something went wrong! - see console for details';
}
