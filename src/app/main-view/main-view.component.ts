import {Component, OnInit, ViewChild} from '@angular/core';
import {TrackDetailsComponent} from '../track-details/track-details.component';
import {CONSTS} from '../consts';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css'],
  providers: [ CONSTS ]
})
export class MainViewComponent implements OnInit {

  selectedTrack;
  tracks;
  searchTerm = '';
  previousSearchTerm = '';
  pageSize = CONSTS.CONFIG_SEARCH_PAGE_SIZE;
  queryPositionCount = 0;
  isWorking = false;
  mCONSTS = CONSTS;
  searchHistoryArr = [];

  @ViewChild('trackDetails')
  private trackDetails: TrackDetailsComponent;

  constructor() { }

  ngOnInit() {
    window['SC'].initialize({
      client_id: this.mCONSTS.SC_CLIENT_ID
    });
    this.setSearchHistoryArrFromLS();
  }

  onSelect(selectedTrack): void {
    if (this.selectedTrack === selectedTrack) {
      return;
    }
    this.trackDetails.clearIframeSrc();
    this.trackDetails.fadeInImage();
    this.selectedTrack = selectedTrack;
  }

  setSearchHistoryArrFromLS(): void {
    let searchHistory = localStorage.getItem("searchHistory");
    if (searchHistory) {
      this.searchHistoryArr = JSON.parse(searchHistory).reverse();
    }
  }

  addSearchHistoryItemToLS(searchTerm): void {
    let searchHistory = localStorage.getItem("searchHistory");
    let arr = [];
    if (searchHistory) {
      arr = JSON.parse(searchHistory);
      if (arr.length === this.mCONSTS.CONFIG_MAX_HISTORY_LENGTH) {
        arr.splice(0,1)
      }
    }
    arr.push(searchTerm);
    localStorage.setItem("searchHistory", JSON.stringify(arr))
  }

  performSearch(searchTerm, isNewSearch, isFromHistory): void {
    if (!searchTerm || this.isWorking) {
      return;
    }

    let ctx = this;
    this.isWorking = true;

    if (isNewSearch) {
      this.queryPositionCount = 0;
      this.previousSearchTerm = searchTerm;
      if (isFromHistory) {
        this.searchTerm = searchTerm;
      } else {
        this.addSearchHistoryItemToLS(searchTerm);
        this.setSearchHistoryArrFromLS();
      }
    } else {
      searchTerm = this.previousSearchTerm;
    }

    this.queryPositionCount++;

    window['SC'].get('/tracks', {
      q: searchTerm,
      limit: this.pageSize,
      linked_partitioning: this.queryPositionCount
    }).then(function (res) {
      ctx.tracks = res.collection;
    }, function (err) {
        console.log(err);
        alert(ctx.mCONSTS.STR_API_ERROR);
    }).finally(function () {
      ctx.isWorking = false;
    });
  }
}
