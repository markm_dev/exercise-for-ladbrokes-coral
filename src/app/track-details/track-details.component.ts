import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-track-details',
  templateUrl: './track-details.component.html',
  styleUrls: ['./track-details.component.css']
})
export class TrackDetailsComponent implements OnInit {

  trackSrc;

  constructor(private domSanitizer : DomSanitizer) { }
  shouldShowImage = false;
  @Input() track: {
    id: string;
  };

  ngOnInit() {

  }

  onImageClick(): void {
    this.setIframeSrc();
  }

  fadeInImage(): void {
    this.shouldShowImage = false;
    let ctx = this;
    setTimeout(function () {
      ctx.shouldShowImage = true;
    },1500)
  }

  clearIframeSrc(): void {
    this.trackSrc = null;
  }

  setIframeSrc(): void {
    let url = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" + this.track.id + "&amp;auto_play=true&amp;show_artwork=true";
    this.trackSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
